import Vue from 'vue'
import Buefy from 'buefy'

Vue.use(Buefy, {
  "css": false,
  "materialDesignIcons": true,
  "materialDesignIconsHRef": "https://use.fontawesome.com/releases/v5.13.1/css/all.css",
  "defaultIconPack": "fa"
})