# ipushpull

First draft for ipushpull. View here -> https://upbeat-roentgen-8b0ba4.netlify.app/

With more time I would have converted the excel styles for a nicer table display.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
